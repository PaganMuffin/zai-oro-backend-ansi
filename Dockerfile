FROM eclipse-temurin:17-jre-jammy
EXPOSE 8080
COPY ./backend.jar ./backend.jar
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "./backend.jar"]

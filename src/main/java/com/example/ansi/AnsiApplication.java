package com.example.ansi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnsiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnsiApplication.class, args);
	}

}
